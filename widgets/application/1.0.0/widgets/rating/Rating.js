/*global
YAHOO
*/

(function () {
    YAHOO.namespace("application.v1_0_0");
    var application = YAHOO.application.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    application.Rating = function() {
        application.Rating.superclass.constructor.apply(this, arguments);
        var me = this;
        this._sketchEvents.forEach(function(eventName){
            me[eventName] = new Toronto.client.Event();
        });
        
        if (this._customEvents){
            this._customEvents.forEach(function(eventName){
                me[eventName] = new Toronto.client.Event(eventName);
            });
        }
    };

    YAHOO.lang.extend(application.Rating, Toronto.framework.DefaultTemplateWidgetImpl, {
        _customEvents: ["star1", "star2", "star3", "star4", "star5"],
        _sketchEvents: [
            "onClick",             
            "onHover", 
            "onMouseEnter", 
            "onMouseLeave", 
            "onMouseOver", 
            "onMouseOut", 
            "onMouseDown", 
            "onMouseUp",
            "onWidgetShow", 
            "onWidgetHide"            
        ],
        // The 'startup' method may be deleted if it is not required, the method from DefaultWidgetImpl will be used
        // Removing the superclass.startup method call may prevent your widget from functioning
        startup: function (widgetContext) {
            application.Rating.superclass.startup.apply(this, arguments);
            var rootWidget = this.getRootWidget();
            var me = this;
            
            if (this._isSketchWidget(rootWidget)){
                // need to call this as defered as we need to wait after the startup is complete on the sketch widget
                setTimeout(function(){
                    me._behaveAsSketchWidget(rootWidget);
                });
            }
        },
        /**
         * Make this widget behaving as sketch widget, this includes adding
         * dynamic attribute setter, default mouse events etc and 
         * delegating it to the root sketch widget
         * 
         * @param {DefaultSketchWidgetImpl} sketchWidget
         *
         */
        _behaveAsSketchWidget: function(sketchWidget){
            var attrs = this.getXMLContext().getAttributes();
            var me = this;
            
            for(var prop in attrs){
               if(attrs.hasOwnProperty(prop)){
                   sketchWidget.attribute(prop, attrs[prop]);
               }
            }    
            
            this._sketchEvents.forEach(function(eventName){
                sketchWidget[eventName].bindHandler(function(ctx){
                    me[eventName].fireEvent({
                        widget:me,
                        action:ctx.action
                    });
                });
            });
        },
        /**
         * Supports show/hide widget including transitions
         * 
         * @param {String} hiddenValue XML value of the hidden attribute
         *
         */
        SETHIDDEN: function(hiddenValue){
            var rootWidget = this.getRootWidget();
            if (this._isSketchWidget(rootWidget)){
                rootWidget.SETHIDDEN.apply(this.getRootWidget(), arguments);
            } else {
                if (hiddenValue === "true"){
                    rootWidget.hide();
                } else {
                    rootWidget.show();
                }
            }
        },
        
        /**
         * Returns true if given widget supports sketch widget's interface
         * 
         * @param {DefaultTemplateWidgetImpl} widget
         * @return {boolean} true if given widget supports sketch widget's interface
         */
        _isSketchWidget: function(widget){
            // JB: note that this can also check whether this widget is instance of
            // Toronto.widget.sketch.v2_0_0.DefaultSketchWidgetImpl
            // althought we would need to check for availability of this class
            // handle different versions etc. We should revisit this default 
            // JS file and see whether default events and attribute setter is still needed
            return this._supportsSketchAttributeSetter(widget) && this._supportsSketchEvents(widget);
        },
        
        /**
         * Returns true if given widget supports sketch widget dynamic attribute setter
         * 
         * @param {DefaultTemplateWidgetImpl} widget
         * @return {boolean} true if given widget supports sketch widget dynamic attribute setter
         */
        _supportsSketchAttributeSetter: function(widget){
            return typeof widget.attribute == "function";
        },
        
        /**
         * Returns true if given widget supports all sketch widget events
         * 
         * @param {DefaultTemplateWidgetImpl} widget
         * @return {boolean} true if widget supports sketch events
         */
        _supportsSketchEvents: function(widget){
            for (var i = 0; i < this._sketchEvents.length; i++) {
                var sketchEvent = widget[this._sketchEvents[i]];
                var isSketchEvent = sketchEvent && sketchEvent instanceof Toronto.client.Event;
                if (!isSketchEvent) return false;
            }
            
            return true;
        },
        
        attribute: function(){
            this.getRootWidget().attribute.apply(this.getRootWidget(), arguments);
        },
        
        getRootWidget: function(){
            return this._widgetContext.getWidgetNode().getChildWidgets()[0];
        },
        getContainerElement: function(){
            return this.getRootWidget().getContainerElement();
        }
    });
})();
